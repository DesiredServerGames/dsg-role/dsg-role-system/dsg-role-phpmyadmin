# Ansible Role: phpMyAdmin

Installs, configure and secure phpMyAdmin in RHEL/Fedora, Debian/Ubuntu.

phpMyAdmin is a simple interface for interacting with MySQL databases via a web browser. It is not necessarily the most secure or efficient method of managing databases, but for those who need a GUI, this one is better than many others. I (geerlingguy) would personally never run it on a production server, nor do I use it myself (I use Sequel Pro or simply interact with the database via CLI/APIs), but it seems many people like it (especially people stuck on a Windows machine with no good MySQL GUIs :).

## Requirements

None

## Role Variables

    phpmyadmin_blowfish: 'lDA60E8JVSY9Z7f=/tOmAM8ZpF56bfoh'
    phpMyAdmin_alias: phpmyadmin
    phpmyadmin_Autorized_ip: [] 
    htpasswd_credentials: []

## Dependencies

  - dsg-role-apache
  - dsg-role-mariadb
  - dsg-role-php

## Example Playbook

    - hosts: utility
      vars_files:
        - vars/main.yml
      roles:
        - { role: dsg-role-phpmyadmin }

*Inside `vars/main.yml`*:

    phpmyadmin_mysql_user: special_user
    phpmyadmin_mysql_password: secure_password_here

## License

MIT / BSD

## Author Information

This role was created in 2021 by Didier MINOTTE.
